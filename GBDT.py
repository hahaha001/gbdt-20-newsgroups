# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import os
import operator
import nltk
from nltk.corpus import stopwords
from sklearn import model_selection
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV

my_path = 'D:\zhaiyisongdata\20_newsgroups'

stop_words = set(stopwords.words('english'))
print("停用词为")
print(stop_words)

vocab = {}
# 加入空白无意义的词语
block_words = ['newsgroups', 'xref', 'path', 'from', 'subject', 'sender', 'organisation', 'apr','gmt', 'last','better','never','every','even','two','good','used','first','need','going','must','really','might','well','without','made','give','look','try','far','less','seem','new','make','many','way','since','using','take','help','thanks','send','free','may','see','much','want','find','would','one','like','get','use','also','could','say','us','go','please','said','set','got','sure','come','lot','seems','able','anything','put', '--', '|>', '>>', '93', 'xref', 'cantaloupe.srv.cs.cmu.edu', '20', '16', "max>'ax>'ax>'ax>'ax>'ax>'ax>'ax>'ax>'ax>'ax>'ax>'ax>'ax>'ax>'", '21', '19', '10', '17', '24', 'reply-to:', 'thu', 'nntp-posting-host:', 're:','25''18'"i'd"'>i''22''fri,''23''>the','references:','xref:','sender:','writes:','1993','organization:']
directory = [f for f in os.listdir('D:/zhaiyisongdata/20_newsgroups/') if not f.startswith('.')]
print(directory)# 打印每个子文件的文件名，即种类

for i in range(len(directory)):

    files = os.listdir('D:/zhaiyisongdata/20_newsgroups/' + directory[i])

    for j in range(len(files)):
        #每个类中的每条数据路径
        path = 'D:/zhaiyisongdata/20_newsgroups/' + directory[i] + '/' + files[j]

        # 打开并读取每个数据文件
        text = open(path, 'r', errors='ignore').read()

        # 读取数据文件中的每个单词
        for word in text.split():
            if len(word) != 1:
                # 看这个单词是否为停用词或者我们加入的空白词
                if not word.lower() in stop_words:
                    if not word.lower() in block_words:
                        # 如果单词已经在词典中该单词的词频就加一
                        if vocab.get(word.lower()) != None:
                            vocab[word.lower()] += 1

                        # 如果单词没有在词典中我们就将该单词放入词典中就将其词频置为1
                        else:
                            vocab[word.lower()] = 1
print(vocab)

# 将形成的单词表排序
sorted_vocab = sorted(vocab.items(), key= operator.itemgetter(1), reverse= True)

feature_list=[]
for key in sorted_vocab:
    feature_list.append(key[0])
feature_list=feature_list[0:2000]

# 创建一个包含2000列的dataframe
df = pd.DataFrame(columns=feature_list)

for i in range(len(directory)):
    files = os.listdir('D:/zhaiyisongdata/20_newsgroups/' + directory[i])

    for j in range(len(files)):
        # 对一个文件生成一行数据，并初始化为0
        df.loc[len(df)] = np.zeros(len(feature_list))

        path = 'D:/zhaiyisongdata/20_newsgroups/' + directory[i] + '/' + files[j]

        text = open(path, 'r', errors='ignore').read()

        # 对每个文件，即df中的每一行，有单词出现则在对应位置词频数加1 
        for word in text.split():
            if word.lower() in feature_list:
                df[word.lower()][len(df) - 1] += 1
"""
# 训练该文件大概花了五十多分钟，为了便于调参时节省时间，就将这个文件保存到本地，因为每次生成的数据都是一样的
# 最后生成的Result.csv文件大概有186M，总共花费时间五十多分钟
df.to_csv('D:/savedf/Result.csv')

# 直接读取生成的Result.csv文件
df=pd.read_csv('D:/savedf/Result.csv')
"""
x = df.values
print(x.shape)

f_list = list(df)

# 生成含有分类信息的y
y = []

for i in range(len(directory)):

    files = os.listdir('D:/zhaiyisongdata/20_newsgroups/' + directory[i])

    for j in range(len(files)):
        y.append(i)

y = np.array(y)
print(y.shape)

x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y, test_size = 0.25, random_state = 0)

# 利用GridSearchCV进行自动化搜索参数选择
params={'learning_rate':np.linspace(0.05,0.5,4), 'max_depth':[x for x in range(3,14,3)],
               'min_samples_leaf':[x for x in range(100,801,250)], 'n_estimators':[x for x in range(20,81,20)],
               'max_features':[x for x in range(7,20,4)]}

model = GradientBoostingClassifier()
grid=GridSearchCV(model, params, cv=10, scoring="accuracy")
grid.fit(x_train, y_train)
# 选择参数加训练模型也耗费了很长时间，大概花了四个多小时
print(grid.best_params_)
# 用最优的参数进行预测
clf=grid.best_estimator_
y_pred = clf.predict(x_test)

train_score = clf.score(x_train, y_train)
test_score = clf.score(x_test, y_test)

print(train_score)
print(test_score)
print("Classification report for GBDT",classification_report(y_test, y_pred))
